﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using CommandLine;
using WikiClientLibrary;
using WikiClientLibrary.Client;
using WikiClientLibrary.Sites;
using NonsensopediaTools.Modules;

namespace NonsensopediaTools
{
    class Program
    {
        private static readonly IModule[] Modules = { new DrzewaLinker(), new NonNewsArchiver() };

        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args).WithParsed(DoWork);
        }

        private static void DoWork(Options opt)
        {
            if (opt.DryRun) Console.WriteLine("Dry run!");
            Console.WriteLine("Łączę...");
            var client = new WikiClient
            {
                ClientUserAgent = "NonsensopediaTools"
            };

            var nonsa = new WikiSite(client, "https://nonsa.pl/api.php");

            try
            {
                nonsa.Initialization.Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Nie umiem się połączyć z Nonsą :'(\n" + ex.Message);
                return;
            }

            try
            {
                var (l, p) = LoginLoader.GetLoginPass("login.txt");
                nonsa.LoginAsync(l, p).Wait();
            }
            catch (WikiClientException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Połączono.");

            foreach (var module in Modules)
            {
                try
                {
                    if (module.IsToBeRun(opt))
                        module.DoWork(nonsa, opt);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }

            Console.WriteLine("Wykonane.");
        }
    }

}
