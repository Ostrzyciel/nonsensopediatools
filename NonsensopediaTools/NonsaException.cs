﻿using System;

namespace NonsensopediaTools
{
    public class NonsaException : Exception
    {
        public NonsaException(string message) : base(message)
        {
        }
    }
}
