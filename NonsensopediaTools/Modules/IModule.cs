﻿using System;
using System.Collections.Generic;
using System.Text;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules
{
    public interface IModule
    {
        bool IsToBeRun(Options opt);

        void DoWork(WikiSite nonsa, Options opt);
    }
}
