﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikiClientLibrary.Generators;
using WikiClientLibrary.Pages;
using WikiClientLibrary.Sites;

namespace NonsensopediaTools.Modules
{
    public class DrzewaLinker : IModule
    {
        public bool IsToBeRun(Options opt) => opt.DrzewaLink;

        public void DoWork(WikiSite nonsa, Options opt)
        {
            Console.WriteLine("Robię {{Drzewa link}}...");
            ConcurrentDictionary<string, string> dict = new ConcurrentDictionary<string, string>();

            var gen = new AllPagesGenerator(nonsa)
            {
                Prefix = "Grafiki – ",
                NamespaceId = 14,
                PaginationSize = 5000
            };
            gen.EnumItemsAsync().ForEachAsync(p =>
                dict.TryAdd(p.Title.Split(':')[1], null)).Wait();

            gen = new AllPagesGenerator(nonsa)
            {
                NamespaceId = 14,
                PaginationSize = 5000
            };
            gen.EnumItemsAsync().ForEachAsync(p =>
            {
                var title = p.Title.Split(':')[1];
                if (title.StartsWith("Grafiki – ")) return;
                if (dict.ContainsKey($"Grafiki – {title}"))
                    dict.TryUpdate($"Grafiki – {title}", title, null);

                var t2 = title.Substring(0, 1).ToLower() + title.Substring(1);
                if (dict.ContainsKey($"Grafiki – {t2}"))
                    dict.TryUpdate($"Grafiki – {t2}", t2, null);
            }).Wait();

            var pary = dict.Where(x => x.Value != null).ToArray();
            Console.WriteLine($"Znalezione pary kategorii: {pary.Length}.");

            bool AddTemplate(string cat)
            {
                try
                {
                    var p = new WikiPage(nonsa, $"Kategoria:{cat}");
                    p.RefreshAsync(PageQueryOptions.FetchContent).Wait();
                    if (p.Content.ToLower().Contains("{{drzewa link}}")) return false;
                    if (p.Content.Trim().StartsWith("{{"))
                        p.Content = "{{Drzewa link}}\n" + p.Content;
                    else p.Content = "{{Drzewa link}}\n\n" + p.Content;

                    Console.WriteLine(cat);

                    if (!opt.DryRun) p.UpdateContentAsync("+{{Drzewa link}}", false, true).Wait();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                    return false;
                }
            }

            int counter = 0;
            object counterLock = new object();

            Parallel.ForEach(pary, para =>
            {
                if (AddTemplate(para.Key))
                    lock (counterLock)
                        counter++;

                if (AddTemplate(para.Value))
                    lock (counterLock)
                        counter++;
            });

            Console.WriteLine("Dodałem {{Drzewa link}} do stron: " + counter);
        }
    }
}
