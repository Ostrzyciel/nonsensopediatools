﻿using System;
using System.Collections.Generic;
using System.Text;
using CommandLine;

namespace NonsensopediaTools
{
    public enum NonNewsArchiveMode
    {
        All,
        OnlyArchive,
        OnlyNewRows
    }

    public class Options
    {
        [Option("dry-run", Default = false, HelpText = "Wykonuje wszystkie operacje, ale nie zapisuje zmian.")]
        public bool DryRun { get; set; }

        [Option("drzewa-link", Default = false, HelpText = "Dodaje szablon {{Drzewa link}} do wymagających tego kategorii.")]
        public bool DrzewaLink { get; set; }

        [Option("nonnews-archive", Default = false, HelpText = "Ogarnia archiwum NonNews.")]
        public bool NonNewsArchive { get; set; }

        [Option("na-ignore-date", Default = false, HelpText = "Pomija sprawdzenie dzisiejszej daty przy robieniu archiwum NonNews.")]
        public bool IgnoreDate { get; set; }

        [Option("na-mode", Default = NonNewsArchiveMode.All, HelpText = "Tryb pracy NonNewsArchive. Dostępne opcje: All, OnlyArchive, OnlyNewRows.")]
        public NonNewsArchiveMode NonNewsArchiveMode { get; set; }
    }
}
