# Z archiwum NonNews

Bot do wstawiania [archiwalnych NonNewsów](https://nonsensopedia.wikia.com/wiki/Nonsensopedia:Z_archiwum_NonNews) na stronę główną. Lata na .NET Core 2.0 więc powinno działać na raczej wszystkim. Binarki pewnie jakieś kiedyś będą.

Żeby zadziałało zrób se w folderze z binarką plik login.txt i w pierwszej linii daj login, w drugiej hasło.

# Ogarniacz kategorii

Bot do ogarniania kategorii.

Póki co robi tylko jedną rzecz: znajduje pary kategorii typu "Grafiki – zwierzęta" i "Zwierzęta", po czym łączy je szablonem [Drzewa link](https://nonsensopedia.wikia.com/wiki/Szablon:Drzewa_link). Jakby ktoś chciał to uruchamiać to oczywiście trzeba pod jakiegoś crona podpiąć i odpalać z opcją --drzewa-link.

# Licencja
Program jest udostępniony na licencji MIT.

Copyright 2019 Ostrzyciel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.